﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestucture.Models
{
   internal partial class posicionMetadata
    {
       
        [Display(Name="Identificador Ubicación")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int idUbicacion { get; set; }


        [Display(Name = "Código Producto")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int codProducto { get; set; }

        [Display(Name = "Cantidad")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        [RegularExpression(@"^\d+$", ErrorMessage = "{0} deber númerico y con dos decimales")]
        public int cantidad { get; set; }

        [Display(Name = "Estado")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int estado { get; set; }

        public virtual ICollection<detalleMovimiento> detalleMovimiento { get; set; }

        [Display(Name = "Producto")]
       
        public virtual Producto Producto { get; set; }

        [Display(Name = "Ubicación")]
        
        public virtual Ubicacion Ubicacion { get; set; }
    }  
    internal partial class rolMetadata
    {
        [Display(Name = "Id. Rol")]
        public int idRol { get; set; }
        [Display(Name = "Rol")]
        public string descripcionRol { get; set; }
    }
    internal partial class productoMetadata
    {
        [Display(Name = "Id. Producto")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int idProducto { get; set; }


        [Display(Name = "Código")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int codProducto { get; set; }

        [Display(Name = "Id Marca")]
        public int idMarca { get; set; }

        [Display(Name = "Nombre Producto")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public string descripcionProducto { get; set; }

        [Display(Name = "Modelo")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public string modeloProducto { get; set; }

        [Display(Name = "Cantidad Máxima")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        [RegularExpression(@"^\d+$", ErrorMessage = "{0} deber númerico y con dos decimales")]
        public int cantMaxima { get; set; }

        [Display(Name = "Cantidad Mínima")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        [RegularExpression(@"^\d+$", ErrorMessage = "{0} deber númerico y con dos decimales")]
        public int cantMinima { get; set; }

        [Display(Name = "Precio")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,2})?$", ErrorMessage = "{0} deber númerico y con dos decimales")]
        public int costo { get; set; }

        [Display(Name = "Estado")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int estado { get; set; }

        [Display(Name = "Detalle Movimiento")]
        public virtual ICollection<detalleMovimiento> detalleMovimiento { get; set; }

        [Display(Name = "Marca")]
        public virtual Marca Marca { get; set; }

        [Display(Name = "Posición")]
        public virtual ICollection<posicion> posicion { get; set; }

        [Display(Name = "Proveedor")]
        public virtual ICollection<Proveedor> Proveedor { get; set; }

    }

    internal partial class ubicacionMetadata
    {
        [Display(Name = "Id Ubicación")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int idUbicacion { get; set; }

        [Display(Name = "Ubicación")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public string descUbicacion { get; set; }

        [Display(Name = "Estado")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int estado { get; set; }

        [Display(Name = "Posición")]
        public virtual ICollection<posicion> posicion { get; set; }

    }

    internal partial class personaMetadata
    {
        [Display(Name = "Id Persona")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int idPersona { get; set; }

        [Display(Name = "Código")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int codPersona { get; set; }

        [Display(Name = "Proveedor")]
        public Nullable<int> codProveedor { get; set; }

        [Display(Name = "Rol")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int idRol { get; set; }

        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public string nombrePersona { get; set; }

        [Display(Name = "Dirección")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public string direccionPersona { get; set; }

        [Display(Name = "Teléfono")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public string telefonoPersona { get; set; }

        [Required(ErrorMessage = "{0} es un dato requerido")]
        [Display(Name = "Contraseña")]
        public string contrasena { get; set; }

        [Display(Name = "Correo Electrónico")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        [DataType(DataType.EmailAddress, ErrorMessage = "{0} no tiene formato válido")]
        public string email { get; set; }

        [Display(Name = "País")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public string pais { get; set; }

        [Display(Name = "Estado")]
        [Required(ErrorMessage = "{0} es un dato requerido")]
        public int estado { get; set; }

        [Display(Name = "Encabezado")]
        public virtual ICollection<encabezadoMovimiento> encabezadoMovimiento { get; set; }

        [Display(Name = "Proveedor")]
        public virtual Proveedor Proveedor { get; set; }

        [Display(Name = "Rol")]
        public virtual Rol Rol { get; set; }     
    }

    internal partial class marcaMetadata
    {
        [Display(Name = "Id Marca")]
        public int idMarca { get; set; }

        [Display(Name = "Marca")]
        public string nombreMarca { get; set; }

        [Display(Name = "Estado")]
        public int estado { get; set; }

        [Display(Name = "Producto")]
        public virtual ICollection<Producto> Producto { get; set; }
    }

    internal partial class proveedorMetadata
    {
        [Display(Name = "Id Proveedor")]
        public int idProveedor { get; set; }

        [Display(Name = "Código Proveedor")]
        public int codProveedor { get; set; }

        [Display(Name = "Nombre del Proveedor")]
        public string nombreProveedor { get; set; }

        [Display(Name = "Dirección")]
        public string direccion { get; set; }

        [Display(Name = "País")]
        public string pais { get; set; }

        [Display(Name = "Estado")]
        public int estado { get; set; }

        [Display(Name = "Encabezado")]
        public virtual ICollection<encabezadoMovimiento> encabezadoMovimiento { get; set; }

        [Display(Name = "Persona")]
        public virtual ICollection<Persona> Persona { get; set; }

        [Display(Name = "Producto")]
        public virtual ICollection<Producto> Producto { get; set; }
       
    }

    internal partial class encabezadoMetadata
    {
        [Display(Name = "N° Movimiento")]
        public int consMovimiento { get; set; }

      /*  [Display(Name = "Id Tipo Movimiento")]
        public int idMovimientoTipo { get; set; }*/

        [Display(Name = "Id Persona")]
        public string codPersona { get; set; }

        [Display(Name = "Cod. Proveedor")]
        public Nullable<int> codProveedor { get; set; }

        [Display(Name = " Cédula Empleado")]
        public string idEmpleado { get; set; }

        [Display(Name = "Fecha")]
        public string fecha { get; set; }

        [Display(Name = "Observaciones")]
        public string documento { get; set; }

        [Display(Name = "Estado")]
        public string estado { get; set; }

        [Display(Name = "Detalle")]
        public virtual ICollection<detalleMovimiento> detalleMovimiento { get; set; }

        [Display(Name = "Tipo de Movimiento")]
        public virtual movimientoTipo movimientoTipo { get; set; }

        [Display(Name = "Cliente")]
        public virtual Persona Persona { get; set; }       

        [Display(Name = "Casa Comercial")]
        public virtual Proveedor Proveedor { get; set; }
    }


    internal partial class movimientoTipoMetadata
    {
        [Display(Name = "N° Movimiento")]
        public int idMovimientoTipo { get; set; }

        [Display(Name = "Movimiento")]
        public string nombreMovimiento { get; set; }

        [Display(Name = "Tipo Movimiento")]
        public int Tipo { get; set; }

        [Display(Name = "Detalle Movimiento")]
        public string descTipo { get; set; }

        [Display(Name = "Estado")]
        public int estado { get; set; }

        [Display(Name = "Encabezado")]
        public virtual ICollection<encabezadoMovimiento> encabezadoMovimiento { get; set; }
    }

}
