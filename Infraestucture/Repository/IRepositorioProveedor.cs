﻿using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestucture.Repository
{
    public interface IRepositorioProveedor
    {
        IEnumerable<Proveedor> GetProveedor();
        IEnumerable<Proveedor> GetLibroByIdPersona(int idPersona);
        Proveedor GetProveedorByCodigo(int codigo);
        Proveedor GetProveedorById(int Id);
        IEnumerable<Proveedor> GetProveedorByNombre(String nombre);
        Proveedor GetProveedorByestado(int estado);
        Proveedor SaveProveedor(Proveedor proveedor);
    }
}
