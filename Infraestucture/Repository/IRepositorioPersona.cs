﻿using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestucture.Repository
{
    public interface IRepositorioPersona
    {
        IEnumerable<Persona> GetPersona();
        Persona GetPersonaById(int id);
        Persona GetPersonaByCod(int cod);

        IEnumerable<Persona> GetPersonaByEstado(int estado);
        Persona GetPersonaByNombre(String nombrePersona);
        Persona GetPersonaByRol(int rol);
        Persona Save(Persona persona);
        Persona GetPersonaLogin(string email, string password);
        void DeletePersona(int id);
    }
}
