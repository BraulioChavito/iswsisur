﻿using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestucture.Repository
{
    public interface IRepositorioProducto
    {
        IEnumerable<Producto> GetProductos();
        IEnumerable<Producto> GetProductosById(int idProducto);
        Producto GetProductoById(int id);
        Producto GetProductoByCodigo(int id);
        Producto GetProductoByProveedores(int id);
        IEnumerable<Producto> GetProductoByProveedor(int idProveedor);
        Producto GetProductoByMarca(String marca);
        IEnumerable<Producto> GetProductoByNombreProducto(String nombre);
        Producto save(Producto producto, string[]selectedProveedores);
        void DeleteProducto(int id);
        IEnumerable<Producto> GetProductoByNameProveedor(String nombreProveedor);

        int totalProductos();
    }
}
