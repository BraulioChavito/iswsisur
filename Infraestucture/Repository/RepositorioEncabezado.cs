﻿using Infraestucture.Models;
using Infraestucture.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestucture.Repository
{
   public class RepositorioEncabezado : IRepositorioEncabezado
    {
        public IEnumerable<encabezadoMovimiento> GetEncabezadoByTipo(int tipo)
        {
            try
            {
                IEnumerable<encabezadoMovimiento> lista = null;
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    //select * from rol
                    lista = ctx.encabezadoMovimiento.Include(y => y.movimientoTipo).
                        Where(x => x.idMovimientoTipo == tipo).ToList();
                }
                return lista;
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw;
            }
        }

        /*  public IEnumerable<encabezadoMovimiento> GetEncabezadoMovimientos()
 {
     try
     {
         IEnumerable<encabezadoMovimiento> lista = null;
         using (MyContext ctx = new MyContext())
         {
             ctx.Configuration.LazyLoadingEnabled = false;
             //select * from rol
             //lista = ctx.encabezadoMovimiento.ToList<encabezadoMovimiento>();

             lista = ctx.encabezadoMovimiento.Include(m => m.movimientoTipo).Include(x => x.Persona).Include(p => p.Proveedor).
                 ToList();
         }
         return lista;
     }
     catch (DbUpdateException dbEx)
     {
         string mensaje = "";
         Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
         throw new Exception(mensaje);
     }
     catch (Exception ex)
     {
         string mensaje = "";
         Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
         throw;
     }
 }*/

        public IEnumerable<encabezadoMovimiento> GetEncabezadoMovimientos()
        {
            List<encabezadoMovimiento> movimientos = null;
            try
            {
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    movimientos = ctx.encabezadoMovimiento.
                               Include("movimientoTipo").
                               Include("Persona").
                               Include("Proveedor").
                               ToList<encabezadoMovimiento>();
                }
                return movimientos;
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
        }


        public encabezadoMovimiento GetMovimientoByCliente(int cedulaCliente)
        {
            throw new NotImplementedException();
        }

        public encabezadoMovimiento GetMovimientoByConsecutivo(int consecutivo)
        {
            encabezadoMovimiento movimientos = null;
            try
            {
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                   movimientos = ctx.encabezadoMovimiento.
                               Include("movimientoTipo").
                               Include("Persona").
                               Include("Proveedor").
                               Include("detalleMovimiento").
                               Include("detalleMovimiento.Producto").
                               Include("detalleMovimiento.Posicion").
                               Where(p => p.consMovimiento == consecutivo).
                               FirstOrDefault<encabezadoMovimiento>();

                }
                return movimientos;

            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
        }

        public encabezadoMovimiento GetMovimientoByEmpleado(int empleado)
        {
            throw new NotImplementedException();
        }

        public encabezadoMovimiento GetMovimientoByFecha(string fecha)
        {
            throw new NotImplementedException();
        }

        public encabezadoMovimiento GetMovimientoByPersona(int idPersona)
        {
            throw new NotImplementedException();
        }

        public encabezadoMovimiento GetMovimientoByProveedor(int cedulaProveedor)
        {
            throw new NotImplementedException();
        }

        public encabezadoMovimiento GetMovimientoByTipo(int tipo)
        {
            throw new NotImplementedException();
        }

        public encabezadoMovimiento Save(encabezadoMovimiento encabezado)
        {
            int resultado = 0;
            encabezadoMovimiento oEncabezado = null;
            try
            {
                // Salvar pero con transacción porque son 2 tablas
                // 1- encabezado
                // 2- encabezadoDetalle 
                using (MyContext ctx = new MyContext())
                {
                    using (var transaccion = ctx.Database.BeginTransaction())
                    {
                        ctx.encabezadoMovimiento.Add(encabezado);
                      resultado = ctx.SaveChanges();
                        foreach (var detalle in encabezado.detalleMovimiento)
                        {
                            detalle.consMovimiento = encabezado.consMovimiento;
                        }
                        foreach (var item in encabezado.detalleMovimiento)
                        {
                            // Busco el producto que está en el detalle por IdLibro
                            Producto oProducto = ctx.Producto.Find(item.codProducto);

                            // Se indica que se alteró
                            ctx.Entry(oProducto).State = EntityState.Modified;
                            // Guardar                         
                            resultado = ctx.SaveChanges();
                        }

                        // Commit 
                        transaccion.Commit();
                    }
                }

                // Buscar la orden que se salvó y reenviarla
                if (resultado >= 0)
                    oEncabezado = GetMovimientoByConsecutivo(encabezado.consMovimiento);


                return oEncabezado;
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw;
            }
        }
    }
}
