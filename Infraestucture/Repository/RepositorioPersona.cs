﻿using Infraestucture.Models;
using Infraestucture.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
//esta referencia a pie
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestucture.Repository
{
    public class RepositorioPersona : IRepositorioPersona
    {
        public void DeletePersona(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Persona> GetPersona()
        {
            try
            {
                IEnumerable<Persona> lista = null;
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    //select * from rol
                    lista = ctx.Persona.Include(x => x.Rol).ToList();
                }
                return lista;
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw;
            }
        }

        public Persona GetPersonaByCod(int cod)
        {
            Persona oPersona = null;
            try
            {
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    oPersona = ctx.Persona.
                        Where(x => x.codPersona == cod).
                        Include(y => y.Rol).Include(l => l.Proveedor).
                        FirstOrDefault<Persona>();
                }
                return oPersona;
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw;
            }
        }

        public IEnumerable<Persona> GetPersonaByEstado(int estado)
        {
            try
            {
                IEnumerable<Persona> lista = null;
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    //select * from rol
                    lista = ctx.Persona.Include(x => x.Rol).Where(x => x.estado == estado).ToList();
                }
                return lista;
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw;
            }
        }

        public Persona GetPersonaById(int id)
        {
            Persona oPersona = null;

            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                oPersona = ctx.Persona.Where(x => x.codPersona == id).Include(y => y.Rol).Include(l => l.Proveedor).FirstOrDefault<Persona>();
            }
            return oPersona;
        }

        public Persona GetPersonaByNombre(string nombrePersona)
        {
            throw new NotImplementedException();
        }

        public Persona GetPersonaByRol(int rol)
        {
            throw new NotImplementedException();
        }

        public Persona GetPersonaLogin(string email, string password)
        {
            Persona oPersona = null;
            try
            {
                //logica para que solo activos entren
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    oPersona = ctx.Persona.
                                 Where(p => p.email.Equals(email) && p.contrasena == password && p.estado == 1).
                                 FirstOrDefault<Persona>();
                }

                if (oPersona != null)
                    oPersona = GetPersonaById(oPersona.codPersona);

                return oPersona;
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw;
            }
        }

        public Persona Save(Persona persona)
        {
            int retorno = 0;
            Persona oPersona = null;
            try
            {
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    oPersona = GetPersonaByCod((int)persona.codPersona);
                    IRepositorioRol _RepositoryRol = new RepositorioRol();

                    if (oPersona == null)
                    {
                        ctx.Persona.Add(persona);
                        retorno = ctx.SaveChanges();
                    }
                    else
                    {
                        ctx.Persona.Add(persona);
                        ctx.Entry(persona).State = EntityState.Modified;
                        retorno = ctx.SaveChanges();
                    }
                }
                if (retorno >= 0)
                    oPersona = GetPersonaByCod((int)persona.codPersona);
                return oPersona;
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw;
            }
        }
    }
}
