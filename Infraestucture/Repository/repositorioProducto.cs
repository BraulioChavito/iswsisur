﻿using Infraestucture.Models;
using Infraestucture.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestucture.Repository
{
   public class repositorioProducto : IRepositorioProducto
    {
        public void DeleteProducto(int id)
        {

            int returno;
            try
            {

                using (MyContext ctx = new MyContext())
                {
                    /* La carga diferida retrasa la carga de datos relacionados,
                     * hasta que lo solicite específicamente.*/
                    ctx.Configuration.LazyLoadingEnabled = false;
                    Producto producto = new Producto()
                    {
                        idProducto = id
                    };
                    ctx.Entry(producto).State = EntityState.Deleted;
                    returno = ctx.SaveChanges();
                }
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw;
            }

        }

        public IEnumerable<Producto> GetProductoByProveedor(int idProveedor)
        {

            IEnumerable<Producto> lista = null;
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                lista = ctx.Producto.Include(c => c.Proveedor).
                    Where(c => c.Proveedor.Any(o => o.codProveedor == idProveedor))
                    .ToList();
            }
            return lista;
        }

        public Producto GetProductoByCodigo(int id)
        {
            Producto producto = null;
            try
            {

                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    producto = ctx.Producto.Find(id);
                }

                return producto;
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw;
            }
        }

        public Producto GetProductoById(int id)
        {
            Producto oProducto = null;
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                oProducto = ctx.Producto.
                    Where(l => l.idProducto == id).Include(l => l.Marca).Include(p => p.posicion).Include("posicion.Ubicacion").Include(x =>x.Proveedor).
                            FirstOrDefault();
            }
            return oProducto;
        }

        public Producto GetProductoByMarca(string marca)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Producto> GetProductoByNombreProducto(string nombre)
        {
            IEnumerable<Producto> lista = null;
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                lista = ctx.Producto.ToList().
                    FindAll(l => l.descripcionProducto.ToLower().Contains(nombre.ToLower()));
            }
            return lista;
        }

        public Producto GetProductoByProveedores(int id)
        {
            Producto oProducto = null;
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                oProducto = ctx.Producto.
                   // Where(l => l.codProveedor == id).Include(l => l.Marca).Include(l => l.Contacto).
                            FirstOrDefault();
            }
            return oProducto;
        }

        public IEnumerable<Producto> GetProductos()
        {
            try
            {
                IEnumerable<Producto> lista = null;
                using (MyContext ctx = new MyContext())
                {
                    ctx.Configuration.LazyLoadingEnabled = false;
                    //select * from rol
                   // lista = ctx.Producto.Include(x =>x.Marca).ToList();
                    lista = ctx.Producto.ToList<Producto>();
                }
                return lista;
            }
            catch (DbUpdateException dbEx)
            {
                string mensaje = "";
                Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw new Exception(mensaje);
            }
            catch (Exception ex)
            {
                string mensaje = "";
                Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
                throw;
            }
        }

        public IEnumerable<Producto> GetProductosById(int idProducto)
        {
            IEnumerable<Producto> lista = null;
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                lista = ctx.Producto.Where(l => l.idProducto == idProducto).Include(l => l.Marca).Include(l => l.posicion).
                    ToList();
            }
            return lista;
        }

        public Producto save(Producto producto,string[]selectedProveedores)
        {
            int retorno = 0;
            Producto oProducto = null;
            //try
            //{
            //    using (MyContext ctx = new MyContext())
            //    {

            //        ctx.Configuration.LazyLoadingEnabled = false;
            //        oProducto = GetProductoById(producto.idProducto);
            //        if (oProducto == null)
            //        {
            //            ctx.Producto.Add(producto);
            //        }
            //        else
            //        {
            //            ctx.Entry(producto).State = EntityState.Modified;
            //        }
            //        retorno = ctx.SaveChanges();
            //    }

            //    if (retorno >= 0)
            //        oProducto = GetProductoById(producto.idProducto);

            //    return oProducto;
            //}
            //catch (DbUpdateException dbEx)
            //{
            //    string mensaje = "";
            //    Log.Error(dbEx, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
            //    throw new Exception(mensaje);
            //}
            //catch (Exception ex)
            //{
            //    string mensaje = "";
            //    Log.Error(ex, System.Reflection.MethodBase.GetCurrentMethod(), ref mensaje);
            //    throw;
            //}
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                oProducto = GetProductoById((int)producto.idProducto);
                IRepositorioProveedor _RepositoryProveedor = new RepositorioProveedor();
           
                if (oProducto == null)
                {

                    //Insertar
                    if (selectedProveedores != null)
                    {
                        producto.Proveedor = new List<Proveedor>();
                        foreach (var proveedor in selectedProveedores)
                        {
                            var proveedorToAdd = _RepositoryProveedor.GetProveedorByCodigo(int.Parse(proveedor));
                            ctx.Proveedor.Attach(proveedorToAdd); //sin esto, EF intentará crear un proveedor
                            producto.Proveedor.Add(proveedorToAdd);// asociar el proveedor existente con el producto
                        }
                    }

                    //                   producto.posicion = new List<posicion>();                 

                    //SaveChanges
                    //guarda todos los cambios realizados en el contexto de la base de datos.
                    ctx.Producto.Add(producto);
                    retorno = ctx.SaveChanges();
                    /*
                    foreach (var pos in oProducto.posicion)
                    {
                        Ubicacion oUbicacion = ctx.Ubicacion.Find(pos.idUbicacion);
                        retorno = ctx.SaveChanges();
                    }*/
                   // ctx.Producto.Add(producto);
                    //retorna número de filas afectadas
                }
                else
                {
                    //Registradas: 1,2,3
                    //Actualizar: 1,3,4

                    //Actualizar Libro
                    ctx.Producto.Add(producto);
                    ctx.Entry(producto).State = EntityState.Modified;
                    retorno = ctx.SaveChanges();
                    //Actualizar Categorias
                    var selectedProveedoresID = new HashSet<string>(selectedProveedores);
                    if (selectedProveedores != null)
                    {
                        ctx.Entry(producto).Collection(p => p.Proveedor).Load();
                        var newProveedorForProducto = ctx.Proveedor
                         .Where(x => selectedProveedoresID.Contains(x.codProveedor.ToString())).ToList();
                        producto.Proveedor = newProveedorForProducto;

                        ctx.Entry(producto).State = EntityState.Modified;
                        retorno = ctx.SaveChanges();
                    }
                   /* foreach (var pos in oProducto.posicion)
                    {
                        Ubicacion oUbicacion = ctx.Ubicacion.Find(pos.idUbicacion);

                        ctx.Entry(oUbicacion).State = EntityState.Modified;
                        retorno = ctx.SaveChanges();
                    }*/
                }
            }

            if (retorno >= 0)
                oProducto = GetProductoById((int)producto.idProducto);
            return oProducto;
        }

        public IEnumerable<Producto> GetProductoByNameProveedor(string nombreProveedor)
        {
            throw new NotImplementedException();
        }

        public int totalProductos()
        {
            int total = 0;
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                total = ctx.Producto.Include(x => x.codProducto).Count();                   
            }
            return total;
        }
    }
}
