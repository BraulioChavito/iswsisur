﻿using appSISUR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appSISUR.ViewModel
{
    public class ubicacionId
    {
        public List<ViewModelUbicaciones> Items { get; private set; }
        public static readonly ubicacionId Instancia;

        static ubicacionId()
        {
            // Si el carrito no está en la sesión, cree uno y guarde los items.
            if (HttpContext.Current.Session["detallePedido"] == null)
            {
                Instancia = new ubicacionId();
                Instancia.Items = new List<ViewModelUbicaciones>();
                HttpContext.Current.Session["detallePedido"] = Instancia;
            }
            else
            {
                // De lo contrario, obténgalo de la sesión.
                Instancia = (ubicacionId)HttpContext.Current.Session["detallePedido"];
            }
        }
        protected ubicacionId() { }

        /**
         * AgregarItem (): agrega un artículo a la compra
         */
        //public String AgregarItem(int ubicacionId)
        //{
        //    String mensaje = "";

        //    // Crear un nuevo artículo para agregar al carrito
        //    viewModelDetallePedido nuevoItem = new viewModelDetallePedido(ubicacionId);
        //    // Si este artículo ya existe en lista de libros, aumente la Cantidad
        //    // De lo contrario, agregue el nuevo elemento a la lista
        //    if (nuevoItem != null)
        //    {
        //        if (Items.Exists(x => x.codProducto == ubicacionId))
        //        {
        //            ViewModelUbicaciones item = Items.Find(x => x.codProducto == ubicacionId);
        //            item.cantidad++;
        //        }
        //        else
        //        {
        //            nuevoItem.cantidad = 1;
        //            Items.Add(nuevoItem);
        //        }
        //        mensaje = SweetAlertHelper.Mensaje("Agregar Producto", "Producto agregado correctamente", SweetAlertMessageType.success);

        //    }
        //    else
        //    {
        //        mensaje = SweetAlertHelper.Mensaje("Agregar Producto", "Producto splicitado no existe", SweetAlertMessageType.warning);
        //    }
        //    return mensaje;
        //}


        ///**
        // * SetItemCantidad(): cambia la Cantidad de un artículo en el carrito
        // */
        //public String SetItemCantidad(int codProducto)
        //{
        //    String mensaje = "";
        //    // Si estamos configurando la Cantidad a 0, elimine el artículo por completo
        //    if (Cantidad == 0)
        //    {
        //        EliminarItem(codProducto);
        //        mensaje = SweetAlertHelper.Mensaje("Producto", "Producto eliminado", SweetAlertMessageType.success);

        //    }
        //    else
        //    {
        //        // Encuentra el artículo y actualiza la Cantidad
        //        ViewModelUbicaciones actualizarItem = new ViewModelUbicaciones(codProducto);
        //        if (Items.Exists(x => x.codProducto == codProducto))
        //        {
        //            ViewModelUbicaciones item = Items.Find(x => x.codProducto == codProducto);
        //            item.cantidad = Cantidad;
        //            mensaje = SweetAlertHelper.Mensaje("Agregar Producto", "Producto actualizado", SweetAlertMessageType.success);
        //        }
        //    }
        //    return mensaje;

        //}

        ///**
        // * EliminarItem (): elimina un artículo del carrito de compras
        // */
        //public String EliminarItem(int codProducto)
        //{
        //    String mensaje = "Producto no existe";
        //    if (Items.Exists(x => x.codProducto == codProducto))
        //    {
        //        var itemEliminar = Items.Single(x => x.codProducto == codProducto);
        //        Items.Remove(itemEliminar);
        //        mensaje = SweetAlertHelper.Mensaje("Producto", "Producto eliminado", SweetAlertMessageType.success);
        //    }
        //    return mensaje;
        //}
        //public int GetCountItems()
        //{
        //    int total = 0;
        //    total = Items.Sum(x => x.cantidad);

        //    return total;
        //}
        //public void eliminarUbicacion()
        //{
        //    Items.Clear();
        //}
    }
}
