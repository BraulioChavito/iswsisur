﻿using Infraestucture.Models;
using ApplicationCore.SERVICES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appSISUR.ViewModel
{
    public class ViewModelPosicion
    {
        public int idUbicacion { get; set; }
        public int codProducto { get; set; }
        public int cantidad { get; set; }
        public int estado { get; set; }

        public virtual Ubicacion Ubicacion { get; set; }

        public ViewModelPosicion(int idUbicacion)
        {
            IServiceUbicacion _ServiceUbicacion = new ServiceUbicacion();
            this.idUbicacion = idUbicacion;
            this.Ubicacion = _ServiceUbicacion.GetUbicacionByID(idUbicacion);
        }
    }
}