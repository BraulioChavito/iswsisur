﻿using appSISUR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appSISUR.ViewModel
{
    public class detallePedido
    {
        public List<viewModelDetallePedido> Items { get; private set; }
        public static readonly detallePedido Instancia;

        static detallePedido()
        {
            // Si el carrito no está en la sesión, cree uno y guarde los items.
            if (HttpContext.Current.Session["detallePedido"] == null)
            {
                Instancia = new detallePedido();
                Instancia.Items = new List<viewModelDetallePedido>();
                HttpContext.Current.Session["detallePedido"] = Instancia;
            }
            else
            {
                // De lo contrario, obténgalo de la sesión.
                Instancia = (detallePedido)HttpContext.Current.Session["detallePedido"];
            }
        }
        protected detallePedido() { }

        /**
         * AgregarItem (): agrega un artículo a la compra
         */
        public String AgregarItem(int productoId)
        {
            String mensaje = "";

            // Crear un nuevo artículo para agregar al carrito
            viewModelDetallePedido nuevoItem = new viewModelDetallePedido(productoId);
            // Si este artículo ya existe en lista de libros, aumente la Cantidad
            // De lo contrario, agregue el nuevo elemento a la lista
            if (nuevoItem != null)
            {
                if (Items.Exists(x => x.codProducto == productoId))
                {
                    viewModelDetallePedido item = Items.Find(x => x.codProducto == productoId);
                    item.cantidad++;
                }
                else
                {
                    nuevoItem.cantidad = 1;
                    Items.Add(nuevoItem);
                }
                mensaje = SweetAlertHelper.Mensaje("Agregar Producto", "Producto agregado correctamente", SweetAlertMessageType.success);

            }
            else
            {
                mensaje = SweetAlertHelper.Mensaje("Agregar Producto", "Producto solicitado no existe", SweetAlertMessageType.warning);
            }
            return mensaje;
        }


        /**
         * SetItemCantidad(): cambia la Cantidad de un artículo en el carrito
         */
        public String SetItemCantidad(int codProducto, int Cantidad)
        {
            String mensaje = "";
            // Si estamos configurando la Cantidad a 0, elimine el artículo por completo
            if (Cantidad == 0)
            {
                EliminarItem(codProducto);
                mensaje = SweetAlertHelper.Mensaje("Producto", "Producto eliminado", SweetAlertMessageType.success);

            }
            else
            {
                // Encuentra el artículo y actualiza la Cantidad
                viewModelDetallePedido actualizarItem = new viewModelDetallePedido(codProducto);
                if (Items.Exists(x => x.codProducto == codProducto))
                {
                    viewModelDetallePedido item = Items.Find(x => x.codProducto == codProducto);
                    item.cantidad = Cantidad;
                    mensaje = SweetAlertHelper.Mensaje("Agregar Producto", "Producto actualizado", SweetAlertMessageType.success);
                }
            }
            return mensaje;

        }

        /**
         * EliminarItem (): elimina un artículo del carrito de compras
         */
        public String EliminarItem(int codProducto)
        {
            String mensaje = "Producto no existe";
            if (Items.Exists(x => x.codProducto == codProducto))
            {
                var itemEliminar = Items.Single(x => x.codProducto == codProducto);
                Items.Remove(itemEliminar);
                mensaje = SweetAlertHelper.Mensaje("Producto", "Producto eliminado", SweetAlertMessageType.success);
            }
            return mensaje;
        }
        public int GetCountItems()
        {
            int total = 0;
            total = Items.Sum(x => x.cantidad);

            return total;
        }
        public void eliminarUbicacion()
        {
            Items.Clear();
        }
    }
}
