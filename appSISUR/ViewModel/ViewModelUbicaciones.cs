﻿using ApplicationCore.SERVICES;
using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appSISUR.ViewModel
{
    public class ViewModelUbicaciones
    {
      
        public int codUbicacion { get; set; }
       
        public string dscUbicacion { get; set; }

        public virtual Ubicacion ubicacion { get; set; }

      

        public ViewModelUbicaciones(int ubicacionId)
        {
            IServiceUbicacion _ServiceUbicacion = new ServiceUbicacion();
            this.codUbicacion = ubicacionId;
            this.ubicacion = _ServiceUbicacion.GetUbicacionByID(ubicacionId);
        }
    }
}