﻿using ApplicationCore.SERVICES;
using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appSISUR.ViewModel
{
    public class viewModelDetallePedido
    {
        public int consMovimiento { get; set; }
        public int codProducto { get; set; }
        public int idUbicacion { get; set; }
        public int cantidad { get; set; }

        public decimal total
        {
            get
            {
                return Producto.costo;
            }
        }

        public virtual encabezadoMovimiento encabezadoMovimiento { get; set; }
        public virtual posicion posicion { get; set; }
        public virtual Producto Producto { get; set; }

        public decimal SubTotal
        {
            get
            {
                return calculoSubtotal();
            }
        }
        private decimal calculoSubtotal()
        {
            return this.total * this.cantidad;
        }

        public viewModelDetallePedido(int productoId)
        {
            IServiceProducto _ServiceProducto = new ServiceProducto();
           this.codProducto = productoId;
            this.Producto = _ServiceProducto.GetProductoById(productoId);
        }
    }
}