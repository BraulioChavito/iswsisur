﻿using appSISUR.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace appSISUR.ViewModel
{
    public class PosicionProducto
    {
        public List<ViewModelPosicion> Items { get; private set; }
        public static readonly PosicionProducto Instancia;

        static PosicionProducto()
        {
            // Si el carrito no está en la sesión, cree uno y guarde los items.
            if (HttpContext.Current.Session["posicionProducto"] == null)
            {
                Instancia = new PosicionProducto();
                Instancia.Items = new List<ViewModelPosicion>();
                HttpContext.Current.Session["posicionProducto"] = Instancia;
            }
            else
            {
                // De lo contrario, obténgalo de la sesión.
                Instancia = (PosicionProducto)HttpContext.Current.Session["posicionProducto"];
            }
        }
        protected PosicionProducto() { }

        /**
         * AgregarItem (): agrega un artículo a la compra
         */
        public String AgregarItem(int ubicacionId)
        {
            String mensaje = "";
            
            // Crear un nuevo artículo para agregar al carrito
            ViewModelPosicion nuevoItem = new ViewModelPosicion(ubicacionId);
            // Si este artículo ya existe en lista de libros, aumente la Cantidad
            // De lo contrario, agregue el nuevo elemento a la lista
            if (nuevoItem != null)
            {
                if (Items.Exists(x => x.idUbicacion == ubicacionId))
                {
                    ViewModelPosicion item = Items.Find(x => x.idUbicacion == ubicacionId);
                    item.cantidad++;
                }
                else
                {
                    nuevoItem.cantidad = 1;
                    Items.Add(nuevoItem);
                }
                mensaje = SweetAlertHelper.Mensaje("Ubicación Producto", "Ubicación agregado al producto", SweetAlertMessageType.success);

            }
            else
            {
                mensaje = SweetAlertHelper.Mensaje("Ubicación Producto", "La ubicación solicitada no existe", SweetAlertMessageType.warning);
            }
            return mensaje;
        }


        /**
         * SetItemCantidad(): cambia la Cantidad de un artículo en el carrito
         */
        public String SetItemCantidad(int ubicacionId, int Cantidad)
        {
            String mensaje = "";
            // Si estamos configurando la Cantidad a 0, elimine el artículo por completo
            if (Cantidad == 0)
            {
                EliminarItem(ubicacionId);
                mensaje = SweetAlertHelper.Mensaje("Ubicación Producto", "Ubicación eliminada", SweetAlertMessageType.success);

            }
            else
            {
                // Encuentra el artículo y actualiza la Cantidad
                ViewModelPosicion actualizarItem = new ViewModelPosicion(ubicacionId);
                if (Items.Exists(x => x.idUbicacion == ubicacionId))
                {
                    ViewModelPosicion item = Items.Find(x => x.idUbicacion == ubicacionId);
                    item.cantidad = Cantidad;
                    mensaje = SweetAlertHelper.Mensaje("Ubicación Producto", "Ubicación actualizada", SweetAlertMessageType.success);

                }
            }
            return mensaje;

        }

        /**
         * EliminarItem (): elimina un artículo del carrito de compras
         */
        public String EliminarItem(int ubicacionId)
        {
            String mensaje = "La ubicación no existe";
            if (Items.Exists(x => x.idUbicacion == ubicacionId))
            {
                var itemEliminar = Items.Single(x => x.idUbicacion == ubicacionId);
                Items.Remove(itemEliminar);
                mensaje = SweetAlertHelper.Mensaje("Ubicación Producto", "Ubicación eliminada", SweetAlertMessageType.success);
            }
            return mensaje;

        }
        public int GetCountItems()
        {
            int total = 0;
            total = Items.Sum(x => x.cantidad);

            return total;
        }
        public void eliminarUbicacion()
        {
            Items.Clear();

        }
    }
}