﻿using ApplicationCore.SERVICES;
using appSISUR.Utils;
using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ApplicationCore.Services;
using appSISUR.Enum;
using appSISUR.Security;

namespace appSISUR.Controllers
{
    public class ProveedorController : Controller
    {
        // GET: Proveedor
        public ActionResult Index()
        {
            IEnumerable<Proveedor> lista = null;
            try
            {
                if (TempData.ContainsKey("NotificationMessage"))
                {
                    ViewBag.NotificationMessage = TempData["NotificationMessage"];
                }
                IServiceProveedor _ServiceProveedor = new ServiceProveedor();
                lista = _ServiceProveedor.GetProveedor();
                ViewBag.listaNombres = _ServiceProveedor.GetProveedoresNombres();
            }
            catch (Exception ex)
            {
                //using web utils
                Log.Error(ex, MethodBase.GetCurrentMethod());
            }
            return View(lista);
        }


        public PartialViewResult Personas(int? id)
        {
            IEnumerable<Proveedor> lista = null;
            IServiceProveedor _ServiceProveedor = new ServiceProveedor();
            if (id != null)
            {
                lista = _ServiceProveedor.GetLibroByIdPersona((int)id);
            }
            return PartialView("_PartialViewProveedor", lista);
        }



        // GET: proveedor/Details/5
        public ActionResult Details(int? id)
        {
            ServiceProveedor _ServiceProveedor = new ServiceProveedor();
            Proveedor Proveedor = null;

            try
            {
                // Si va null
                if (id == null)
                {
                    return RedirectToAction("Index");
                }

                Proveedor = _ServiceProveedor.GetProveedorById(id.Value);
                if (Proveedor == null)
                {
                    TempData["Message"] = "No existe el Proveedor solicitado";
                    TempData["Redirect"] = "Proveedor";
                    TempData["Redirect-Action"] = "Index";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }
                ViewBag.Titulo = "Detalle Proveedor";
                return View(Proveedor);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Proveedor";
                TempData["Redirect-Action"] = "Index";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }
        [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult buscarProveedorxNombre(string filtro)
        {
            IEnumerable<Proveedor> lista = null;
            IServiceProveedor _ServiceProveedor = new ServiceProveedor();

            // Error porque viene en blanco 
            if (string.IsNullOrEmpty(filtro))
            {
                lista = _ServiceProveedor.GetProveedor();
            }
            else
            {
                lista = _ServiceProveedor.GetProveedorByNombre(filtro);
            }


            // Retorna un Partial View
            return PartialView("_PartialViewProveedorAdmin", lista);
        }

        public ActionResult AjaxDetails(int id)
        {

            IServiceProveedor _ServiceProveedor = new ServiceProveedor();
            Proveedor proveedor = _ServiceProveedor.GetProveedorById(id);
            return PartialView("_PartialViewProveedorDetails", proveedor);
        }
        private SelectList listaPersonas(int idPersona = 0)
        {
            //Lista de autores
            IServicePersona _ServicePersona = new ServicePersona();
            IEnumerable<Persona> listaPersonas = _ServicePersona.GetPersona();
            //Autor SelectAutor = listaAutores.Where(c => c.IdAutor == idAutor).FirstOrDefault();
            return new SelectList(listaPersonas, "idPersona", "nombrePersona", idPersona);
        }


        [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult Create()
        {
            ViewBag.NotificationMessage = TempData["NotificationMessage"];
            //ViewBag.IdPersona = listaPersonas();
            return View();
        }


        // GET: proveedor/Edit/5
        [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult Edit(int? id)
        {
            ServiceProveedor _ServiceProveedor = new ServiceProveedor();
            Proveedor proveedor = null;
            try
            {
                if (id == null)
                {
                    return RedirectToAction("Index");
                }

                proveedor = _ServiceProveedor.GetProveedorById(id.Value);
                if (proveedor == null)
                {
                    TempData["Message"] = "No existe el Proveedor";
                    TempData["Redirect"] = "Proveedor";
                    TempData["Redirect-Action"] = "Index";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }

                //ViewBag.IdPersona = listaPersonas();                
                return View(proveedor);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Libro";
                TempData["Redirect-Action"] = "IndexAdmin";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }

        [CustomAuthorize((int)Roles.Administrador)]
        // POST: Proveedor/Edit/5
        [HttpPost]
        public ActionResult Save(Proveedor proveedor)
        {
            IServiceProveedor _ServiceProveedor = new ServiceProveedor();
            try
            {
                if (ModelState.IsValid)
                {
                    Proveedor oProveedorI = _ServiceProveedor.SaveProveedor(proveedor);
                }
                else
                {
                    // Valida Errores si Javascript está deshabilitado
                    Util.Util.ValidateErrors(this);                  
                    return View("Create", proveedor);
                }
                // return RedirectToAction("IndexAdmin");
                TempData["NotificationMessage"] = SweetAlertHelper.Mensaje("Proveedor", "Proveedor registrado satisfactoriamente!", SweetAlertMessageType.success);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Libro";
                TempData["Redirect-Action"] = "IndexAdmin";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }

        // GET: proveedor/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: proveedor/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}