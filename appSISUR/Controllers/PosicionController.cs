﻿using ApplicationCore.SERVICES;
using appSISUR.Utils;
using appSISUR.ViewModel;
using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace appSISUR.Controllers
{
    public class PosicionController : Controller
    {
        // GET: CPosicion
        public ActionResult Index()
        {
            IEnumerable<posicion> lista = null;
            try
            {
                IServicePosicion _ServicePosicion = new ServicePosicion();
                lista = _ServicePosicion.GetPsicion();
                ViewBag.Posicion = PosicionProducto.Instancia.Items;
            }
            catch (Exception ex)
            {
                //using web utils
                Log.Error(ex, MethodBase.GetCurrentMethod());
            }
            ViewBag.titulo = "Listado de Posiciones";
            return View(lista);
        }
        public ActionResult IndexAdmin()
        {
            IEnumerable<posicion> lista = null;
            try
            {
                IServicePosicion _ServicePosicion = new ServicePosicion();
                lista = _ServicePosicion.GetPsicion();
            }
            catch (Exception ex)
            {
                //using web utils
                Log.Error(ex, MethodBase.GetCurrentMethod());
            }
            return View(lista);
        }


        // GET: CPosicion/Details/5
        public ActionResult Details(int? id)
        {
            ServicePosicion _ServicePosicion = new ServicePosicion();
            posicion Posicion = null;

            try
            {
                // Si va null
                if (id == null)
                {
                    return RedirectToAction("Index");
                }

                Posicion = _ServicePosicion.getDetalleById(id.Value);
                if (Posicion == null)
                {
                    TempData["Message"] = "No existe el Posicion solicitado";
                    TempData["Redirect"] = "Posicion";
                    TempData["Redirect-Action"] = "IndexAdmin";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }
                return View(Posicion);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Posicion";
                TempData["Redirect-Action"] = "IndexAdmin";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }
        public ActionResult AjaxDetails(int id)
        {

            IServicePosicion _ServicePosicion = new ServicePosicion();
            posicion posicion = _ServicePosicion.getDetalleById(id);
            return PartialView("_PartialViewPosicionDetails", posicion);
        }
        private ActionResult DetallePosicion()
        {

            return PartialView("_DetallePosicion", PosicionProducto.Instancia.Items);
        }

        public ActionResult actualizarCantidad(int idUbicacion, int cantidad)
        {
            ViewBag.Posicion = PosicionProducto.Instancia.Items;
            TempData["NotiCarrito"] = PosicionProducto.Instancia.SetItemCantidad(idUbicacion, cantidad);
            TempData.Keep();
            return PartialView("_DetallePosicion", PosicionProducto.Instancia.Items);

        }

        public ActionResult agregarPosicion(int? idUbicacion)
        {
            int cantidadLibros = PosicionProducto.Instancia.Items.Count();
            ViewBag.NotiCarrito = PosicionProducto.Instancia.AgregarItem((int)idUbicacion);
            return PartialView("_ubicacionCantidad");

        }

        //Actualizar solo la cantidad de libros que se muestra en el menú
        public ActionResult actualizarUbicacionCantidad()
        {
            if (TempData.ContainsKey("NotiCarrito"))
            {
                ViewBag.NotiCarrito = TempData["NotiCarrito"];
            }
            int cantidadLibros = PosicionProducto.Instancia.Items.Count();
            return PartialView("_ubicacionCantidad");

        }
        public ActionResult eliminarUbicacion(int? idUbicacion)
        {
            ViewBag.NotificationMessage = PosicionProducto.Instancia.EliminarItem((int)idUbicacion);
            return PartialView("_DetallePosicion", PosicionProducto.Instancia.Items);
        }

        private SelectList listaProductos(int idProducto = 0)
        {
            //Lista de roles
            IServiceProducto _ServiceProducto = new ServiceProducto();
            IEnumerable<Producto> listaProductos = _ServiceProducto.GetProductos();

            return new SelectList(listaProductos, "codProducto", "descripcionProducto", idProducto);
        }

        private SelectList listUbic(int idUbicacion = 0)
        {
            //Lista de roles
            IServiceUbicacion _ServiceUbicacion = new ServiceUbicacion();
            IEnumerable<Ubicacion> listUbic = _ServiceUbicacion.GetUbicacion();
            return new SelectList(listUbic, "idUbicacion", "descUbicacion", idUbicacion);
        }


        /*  private MultiSelectList listaUbicaciones(ICollection<Ubicacion> ubicaciones)
          {
              //Lista de Categorias
              IServiceUbicacion _ServiceUbicacion = new ServiceUbicacion();
              IEnumerable<Ubicacion> listaUbicaciones = _ServiceUbicacion.GetUbicacion();
              int[] listaUbicacionesSelect = null;

              if (ubicaciones != null)
              {

                  listaUbicacionesSelect = ubicaciones.Select(c => c.idUbicacion).ToArray();
              }

              return new MultiSelectList(listaUbicaciones, "idUbicacion", "descUbicacion", listaUbicacionesSelect);

          }*/

        // GET: CPosicion/Create
        public ActionResult Create()
        {
            ViewBag.IdProducto = listaProductos();
            ViewBag.IdUbicacion = listUbic();
            ViewBag.Posicion = PosicionProducto.Instancia.Items;
            return View();
        }



        // GET: CPosicion/Edit/5
        public ActionResult Edit(int? id)
        {
            IServicePosicion _ServicePosicion = new ServicePosicion();
            posicion pos = null;

            try
            {
                // Si va null
                if (id == null)
                {
                    return RedirectToAction("Index");
                }

                pos = _ServicePosicion.getDetalleById(id.Value);
                if (pos == null)
                {
                    TempData["Message"] = "No existe la posición solicitada";
                    TempData["Redirect"] = "Posicion";
                    TempData["Redirect-Action"] = "Index";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }
                //Lista de autores
                ViewBag.IdProducto = listaProductos(pos.codProducto);
                ViewBag.IdUbicacion = listUbic(pos.idUbicacion);
                return View(pos);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Posicion";
                TempData["Redirect-Action"] = "Index";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }

        [HttpPost]
        public ActionResult Save(posicion pos)
        {           
            IServicePosicion _ServicePosicion = new ServicePosicion();
            try
            {
                if (ModelState.IsValid)
                {
                    posicion oPosicionI = _ServicePosicion.Save(pos);
                }
                else
                {
                    // Valida Errores si Javascript está deshabilitado
                    Util.Util.ValidateErrors(this);
                    ViewBag.IdProducto = listaProductos(pos.codProducto);
                    ViewBag.IdUbicacion = listUbic(pos.idUbicacion);
                    //ViewBag.idUbicacion = listaUbicaciones(pos.Ubicacion);
                    return View("Create", pos);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Posicion";
                TempData["Redirect-Action"] = "Index";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }           
        }

        // GET: CPosicion/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CPosicion/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
