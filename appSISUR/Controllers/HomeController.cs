﻿using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace appSISUR.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            int total = 0;
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                total = ctx.Producto.Count();
            }
            ViewBag.total = total;

            int totalSalidas = 0;
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                totalSalidas = ctx.encabezadoMovimiento.Where(x=> x.idMovimientoTipo == 1).Count();
            }
            ViewBag.totalSalidas = totalSalidas;

            int totalIngresos = 0;
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                totalIngresos = ctx.encabezadoMovimiento.Where(x => x.idMovimientoTipo != 1).Count();
            }
            ViewBag.totalIngresos = totalIngresos;

            int totalUsuarios = 0;
            using (MyContext ctx = new MyContext())
            {
                ctx.Configuration.LazyLoadingEnabled = false;
                totalUsuarios = ctx.Persona.Count();
            }
            ViewBag.totalUsuarios = totalUsuarios;


            return View("Index");
        }



        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}