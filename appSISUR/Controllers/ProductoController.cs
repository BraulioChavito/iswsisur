﻿using ApplicationCore.SERVICES;
using appSISUR.Enum;
using appSISUR.Security;
using appSISUR.Utils;
using appSISUR.ViewModel;
using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace appSISUR.Controllers
{
    public class ProductoController : Controller
    {
        // GET: Producto
        public ActionResult Index()
        {
            IEnumerable<Producto> lista = null;
            try
            {
                if (TempData.ContainsKey("NotificationMessage"))
                {
                    ViewBag.NotificationMessage = TempData["NotificationMessage"];
                }
                IServiceProducto _ServiceProducto = new ServiceProducto();
                lista = _ServiceProducto.GetProductos();
                // Lista autocompletando productos
                ViewBag.listaNombres = _ServiceProducto.GetProductoNombres();
                // ViewBag.Posicion = PosicionProducto.Instancia.Items;
            }
            catch (Exception ex)
            {
                //using web utils
                Log.Error(ex, MethodBase.GetCurrentMethod());
            }
          
            return View(lista);
        }

        // GET: Producto/Details/5
        public ActionResult Details(int? id)
        {
            ServiceProducto _ServiceProducto = new ServiceProducto();
            Producto Producto = null;

            try
            {
                // Si va null
                if (id == null)
                {
                    return RedirectToAction("Index");
                }

                Producto = _ServiceProducto.GetProductoById(id.Value);
                if (Producto == null)
                {
                    TempData["Message"] = "No existe el Producto solicitado";
                    TempData["Redirect"] = "Producto";
                    TempData["Redirect-Action"] = "IndexAdmin";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }
                ViewBag.titulo = "Detalle de Productos";
                return View(Producto);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Producto";
                TempData["Redirect-Action"] = "IndexAdmin";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }
        public ActionResult AjaxDetails(int id)
        {

            IServiceProducto _ServiceProducto = new ServiceProducto();
            Producto producto = _ServiceProducto.GetProductoById(id);
            return PartialView("_PartialViewProductoDetails", producto);
        }
        private ActionResult DetallePosicion()
        {

            return PartialView("_DetallePosicion", PosicionProducto.Instancia.Items);
        }

        public ActionResult actualizarCantidad(int idUbicacion, int cantidad)
        {
            ViewBag.Posicion = PosicionProducto.Instancia.Items;
            TempData["NotiCarrito"] = PosicionProducto.Instancia.SetItemCantidad(idUbicacion, cantidad);
            TempData.Keep();
            return PartialView("_DetallePosicion", PosicionProducto.Instancia.Items);

        }

        public ActionResult agregarPosicion(int? idUbicacion)
        {
            int cantidadLibros = PosicionProducto.Instancia.Items.Count();
            ViewBag.NotiCarrito = PosicionProducto.Instancia.AgregarItem((int)idUbicacion);
            return PartialView("_ubicacionCantidad");

        }

        //Actualizar solo la cantidad de libros que se muestra en el menú
        public ActionResult actualizarUbicacionCantidad()
        {
            if (TempData.ContainsKey("NotiCarrito"))
            {
                ViewBag.NotiCarrito = TempData["NotiCarrito"];
            }
            int cantidadLibros = PosicionProducto.Instancia.Items.Count();
            return PartialView("_ubicacionCantidad");

        }
        public ActionResult eliminarUbicacion(int? idUbicacion)
        {
            ViewBag.NotificationMessage = PosicionProducto.Instancia.EliminarItem((int)idUbicacion);
            return PartialView("_DetallePosicion", PosicionProducto.Instancia.Items);
        }


        public ActionResult mostrarTotal()
        {
            IServiceProducto _ServiceProducto = new ServiceProducto();
            return  ViewBag.total = _ServiceProducto.totalProductos();
        }


        public ActionResult DetailsProveedor(int? id)
        {
            ServiceProducto _ServiceProducto = new ServiceProducto();
            Producto Producto = null;

            try
            {
                // Si va null
                if (id == null)
                {
                    return RedirectToAction("Index");
                }

                Producto = _ServiceProducto.GetProductoByProveedores(id.Value);
                if (Producto == null)
                {
                    TempData["Message"] = "No existe el Producto solicitado";
                    TempData["Redirect"] = "Producto";
                    TempData["Redirect-Action"] = "IndexAdmin";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }
                ViewBag.titulo = "Detalle de Productos";
                return View(Producto);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Producto";
                TempData["Redirect-Action"] = "IndexAdmin";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }
        //Solo tiene acceso el Admin
        [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult buscarProductoxNombre(string filtro)
        {
            IEnumerable<Producto> lista = null;
            IServiceProducto _ServiceProducto = new ServiceProducto();

            // Error porque viene en blanco 
            if (string.IsNullOrEmpty(filtro))
            {
                lista = _ServiceProducto.GetProductos();
            }
            else
            {
                lista = _ServiceProducto.GetProductoByNombreProducto(filtro);
            }


            // Retorna un Partial View
            return PartialView("_PartialViewProductoAdmin", lista);
        }

        private SelectList listaMarcas(int idMarca = 0)
        {
            //Lista de roles
            IServiceMarca _ServiceMarca = new ServiceMarca();
            IEnumerable<Marca> listaMarcas = _ServiceMarca.GetMarca();
            //Autor SelectAutor = listaAutores.Where(c => c.IdAutor == idAutor).FirstOrDefault();
            return new SelectList(listaMarcas, "idMarca", "nombreMarca", idMarca);
        }

        private MultiSelectList listaProveedores(ICollection<Proveedor> proveedores)
        {
            //Lista de Proveedores
            IServiceProveedor _ServiceProveedor = new ServiceProveedor();
            IEnumerable<Proveedor> listaProveedores = _ServiceProveedor.GetProveedor();
            int[] listaProveedoresSelect = null;

            if (proveedores != null)
            {

                listaProveedoresSelect = proveedores.Select(c => c.codProveedor).ToArray();
            }

            return new MultiSelectList(listaProveedores, "codProveedor", "nombreProveedor", listaProveedoresSelect);

        }

        private MultiSelectList listaUbicaciones(ICollection<Ubicacion> ubicaciones)
        {
            
            IServiceUbicacion _ServiceUbicacion = new ServiceUbicacion();
            IEnumerable<Ubicacion> listaUbicaciones = _ServiceUbicacion.GetUbicacion();
            int[] listaUbicacionesSelect = null;

            if (ubicaciones != null)
            {

                listaUbicacionesSelect = ubicaciones.Select(c => c.idUbicacion).ToArray();
            }

            return new MultiSelectList(listaUbicaciones, "idUbicacion", "descUbicacion", listaUbicacionesSelect);

        }

        [CustomAuthorize((int)Roles.Administrador)]
        // GET: Producto/Create
        public ActionResult Create()
        {
            //Lista de marcas
            ViewBag.idMarca = listaMarcas();
            ViewBag.idProveedor = listaProveedores(null);
            ViewBag.idUbicacion = listaUbicaciones(null);
            //Lista de proveedores
          //  ViewBag.idProveedor = listaProveedores();

            ViewBag.NotificationMessage = TempData["NotificationMessage"];
            return View();
        }

        //private SelectList listaProveedores(int idProveedor = 0)
        //{
        //    //Lista de Proveedores
        //    IServiceProveedor _ServiceProveedor = new ServiceProveedor();
        //    IEnumerable<Proveedor> listaProveedores = _ServiceProveedor.GetProveedor();
        //    return new SelectList(listaProveedores, "codProveedor", "nombreProveedor", idProveedor);
        //}
        // POST: Producto/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Producto/Edit/5
        [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult Edit(int? id)
        {
            IServiceProducto _ServiceProducto = new ServiceProducto();
            Producto producto = null;
        
            try
            {
                // Si va null
                if (id == null)
                {
                    return RedirectToAction("Index");
                }

               
                producto = _ServiceProducto.GetProductoById(id.Value);

                if (producto == null)
                {
                    TempData["Message"] = "No existe el producto solicitado";
                    TempData["Redirect"] = "Producto";
                    TempData["Redirect-Action"] = "Index";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }
                //Lista de marcas
                ViewBag.idMarca = listaMarcas(producto.idMarca);
                ViewBag.idProveedor = listaProveedores(producto.Proveedor);
                //Lista de proveedores
                // ViewBag.idProveedor = listaProveedores(producto.idProveedor);

                return View(producto);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Producto";
                TempData["Redirect-Action"] = "IndexAdmin";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }

       


        [HttpPost]
        public ActionResult Save(Producto producto, string[] selectedProveedores)
        {
            IServiceProducto _ServiceProducto = new ServiceProducto();
            String mensaje = "";
            try
            {
                if (ModelState.IsValid)
                {
                  /*  var listaDetalle = PosicionProducto.Instancia.Items;
                    foreach (var item in listaDetalle)
                    {
                        posicion posDetalle = new posicion();
                        posDetalle.idUbicacion = item.idUbicacion;
                        posDetalle.codProducto = item.codProducto;
                        posDetalle.cantidad = item.cantidad;
                        producto.posicion.Add(posDetalle);
                    }*/
                    Producto oProductoI = _ServiceProducto.save(producto,selectedProveedores);
                }
                else
                {
                    // Valida Errores si Javascript está deshabilitado
                    Util.Util.ValidateErrors(this);
                    ViewBag.idMarca = listaMarcas();
                    ViewBag.idProveedor = listaProveedores(producto.Proveedor);
                  

                    return View("Create", producto);
                }
                // Limpia el Carrito de compras
                PosicionProducto.Instancia.eliminarUbicacion();
                TempData["NotificationMessage"] = SweetAlertHelper.Mensaje("Producto", "Producto guardado satisfactoriamente!", SweetAlertMessageType.success);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Producto";
                TempData["Redirect-Action"] = "Index";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }

        // POST: Producto/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Producto/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Producto/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
