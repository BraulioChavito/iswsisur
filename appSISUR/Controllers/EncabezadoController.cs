﻿using ApplicationCore.SERVICES;
using appSISUR.Security;
using appSISUR.Utils;
using appSISUR.ViewModel;
using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

using System.IO;
using appSISUR.Enum;

namespace appSISUR.Controllers
{
    public class EncabezadoController : Controller
    {
        [CustomAuthorize((int)Roles.Administrador)]
        // GET: Encabezado
        public ActionResult Index()
        {
            IEnumerable<encabezadoMovimiento> lista = null;
            try
            {
                if (TempData.ContainsKey("NotificationMessage"))
                {
                    ViewBag.NotificationMessage = TempData["NotificationMessage"];
                }
                IServicoEncabezado _ServiceEncabezado = new ServicioEncabezado();
                lista = _ServiceEncabezado.GetEncabezadoMovimientos();
                ViewBag.DetalleOrden = detallePedido.Instancia.Items;
            }
            catch (Exception ex)
            {
                //using web utils
                Log.Error(ex, MethodBase.GetCurrentMethod());
            }
            ViewBag.Titulo = "Transacciones";
            return View(lista);

        }
        [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult Details(int? id)
        {
            ServicioEncabezado _ServiceEncabezado = new ServicioEncabezado();
            encabezadoMovimiento oEncabezado = null;
            try
            {
                // Si va null
                if (id == null)
                {
                    return RedirectToAction("Index");
                }

                oEncabezado = _ServiceEncabezado.GetMovimientoByConsecutivo(id.Value);
                if (id == null)
                {
                    TempData["Message"] = "No existe el Encabezado solicitado";
                    TempData["Redirect"] = "Encabezado";
                    TempData["Redirect-Action"] = "IndexAdmin";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }
                ViewBag.Titulo = "Detalle Transacciones";
                return View(oEncabezado);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Encabezado";
                TempData["Redirect-Action"] = "IndexAdmin";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }

        public ActionResult AjaxDetails(int id)
        {

            IServicoEncabezado _ServiceEncabezado = new ServicioEncabezado();
            encabezadoMovimiento encabezado = _ServiceEncabezado.GetMovimientoByConsecutivo(id);
            return PartialView("_PartialViewEncabezadoDetails", encabezado);
        }
        private ActionResult DetallePedido()
        {
            return PartialView("_DetallePedido", detallePedido.Instancia.Items);
        }

        public ActionResult actualizarCantidad(int codProducto, int cantidad)
        {
            ViewBag.DetalleOrden = detallePedido.Instancia.Items;
            TempData["NotiCarrito"] = detallePedido.Instancia.SetItemCantidad(codProducto, cantidad);
            TempData.Keep();
            return PartialView("_DetallePedido", detallePedido.Instancia.Items);
        }


        //nombre
        private SelectList listaMovimientos(int idMovimiento = 0)
        {
            IServiceMovimiento _ServiceEncabezado = new ServiceMovimiento();
            IEnumerable<movimientoTipo> listaMovimientos = _ServiceEncabezado.GetMovimientoTipo();
            //Autor SelectAutor = listaAutores.Where(c => c.IdAutor == idAutor).FirstOrDefault();
            return new SelectList(listaMovimientos, "idMovimientoTipo", "nombreMovimiento", idMovimiento);
        }

        //tipo
        private SelectList listaTiposMovimientos(int idMovimientoTipo = 0)
        {
            IServiceMovimiento _ServiceEncabezado = new ServiceMovimiento();
            IEnumerable<movimientoTipo> listaTiposMovimientos = _ServiceEncabezado.GetMovimientoTipo();
            return new SelectList(listaTiposMovimientos, "idMovimientoTipo", "descTipo", idMovimientoTipo);
        }

        //ubicaciones
        private SelectList listaUbicaciones(int id = 0)
        {
            IServiceUbicacion _ServiceUbicacion = new ServiceUbicacion();
            IEnumerable<Ubicacion> listaUbicaciones = _ServiceUbicacion.GetUbicacion();
            return new SelectList(listaUbicaciones, "idUbicacion", "descUbicacion", id);
        }



        //codpersona
        private SelectList listaPersonas(int idPersona = 0)
        {
            //Lista de autores
            IServicePersona _ServicePersona = new ServicePersona();
            IEnumerable<Persona> listaPersonas = _ServicePersona.GetPersona();
            return new SelectList(listaPersonas, "codPersona", "nombrePersona", idPersona);
        }

        //codProveedor
        private SelectList listaProveedores(int idProveedor = 0)
        {
            IServiceProveedor _ServiceProveedor = new ServiceProveedor();
            IEnumerable<Proveedor> listaProveedores = _ServiceProveedor.GetProveedor();
            return new SelectList(listaProveedores, "codProveedor", "nombreProveedor", idProveedor);
        }

        public ActionResult agregarProducto(int? idProducto)
        {
            int cantidadProducto = detallePedido.Instancia.Items.Count();
            ViewBag.NotiCarrito = detallePedido.Instancia.AgregarItem((int)idProducto);
            return PartialView("_ProductoCantidad");
        }

        public ActionResult actualizarProductoCantidad()
        {
            if (TempData.ContainsKey("NotiCarrito"))
            {
                ViewBag.NotiCarrito = TempData["NotiCarrito"];
            }
            int cantidadProductos = detallePedido.Instancia.Items.Count();
            return PartialView("_ProductoCantidad");
        }
        public ActionResult eliminarProducto(int? codProducto)
        {
            ViewBag.NotificationMessage = detallePedido.Instancia.EliminarItem((int)codProducto);
            return PartialView("_DetallePedido", detallePedido.Instancia.Items);
        }
        [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult Create()
        {
            ViewBag.codPersona = listaPersonas();
            ViewBag.idEmpleado = listaPersonas();
            ViewBag.codProveedor = listaProveedores();
            ViewBag.idMovimientoTipo = listaMovimientos();
            ViewBag.DetalleOrden = detallePedido.Instancia.Items;
            return View();
        }
        [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult Edit(int? id)
        {
            ServicioEncabezado _ServiceEncabezado = new ServicioEncabezado();
            encabezadoMovimiento encabezado = null;
            try
            {
                if (id == null)
                {
                    return RedirectToAction("Index");
                }
                encabezado = _ServiceEncabezado.GetMovimientoByConsecutivo(id.Value);
                if (encabezado == null)
                {
                    TempData["Message"] = "No existe el Registro";
                    TempData["Redirect"] = "Encabezado";
                    TempData["Redirect-Action"] = "Index";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }
                ViewBag.codPersona = listaPersonas(encabezado.codPersona);
                ViewBag.idEmpleado = listaPersonas(encabezado.codPersona);
                ViewBag.codProveedor = listaProveedores(encabezado.Proveedor.codProveedor);
                ViewBag.idMovimientoTipo = listaMovimientos(encabezado.idMovimientoTipo);
                return View(encabezado);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Encabezado";
                TempData["Redirect-Action"] = "Index";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }
        [CustomAuthorize((int)Roles.Administrador)]
        [HttpPost]
        public ActionResult Save(encabezadoMovimiento encabezado)
        {
            IServicoEncabezado _ServiceEncabezado = new ServicioEncabezado();

            String mensaje = "";
            try
            {
                if (ModelState.IsValid)
                {
                    // Si no existe la sesión no hay nada
                    if (detallePedido.Instancia.Items.Count() <= 0)
                    {
                        // Validaciones de datos requeridos.
                        TempData["NotificationMessage"] = Utils.SweetAlertHelper.Mensaje("Encabezado", "Seleccione los productos a ordenar", SweetAlertMessageType.warning);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        var listaDetalle = detallePedido.Instancia.Items;

                        foreach (var item in listaDetalle)
                        {
                            detalleMovimiento encabezadoMov = new detalleMovimiento();
                            encabezadoMov.codProducto = item.codProducto;
                            //encabezadoMov.Producto.descripcionProducto = item.Producto.descripcionProducto;
                     //       encabezadoMov.idUbicacion = item.posicion.idUbicacion;
                            encabezadoMov.cantidad = item.cantidad;

                            encabezado.detalleMovimiento.Add(encabezadoMov);
                        }
                    }
                    encabezadoMovimiento oEncabezadoI = _ServiceEncabezado.Save(encabezado);
                }
                else
                {
                    // Valida Errores si Javascript está deshabilitado
                    Util.Util.ValidateErrors(this);
                    ViewBag.codPersona = listaPersonas();
                    ViewBag.idEmpleado = listaPersonas();
                    ViewBag.codProveedor = listaProveedores();
                    ViewBag.idMovimientoTipo = listaMovimientos();
                    return View("Create", encabezado);
                }
                detallePedido.Instancia.eliminarUbicacion();
                TempData["NotificationMessage"] = SweetAlertHelper.Mensaje("Producto", "Producto guardado satisfactoriamente!", SweetAlertMessageType.success);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Encabezado";
                TempData["Redirect-Action"] = "Index";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }

        //Solo tiene acceso el Admin
        [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult buscarXTipo(int filtro)
        {
            IEnumerable<encabezadoMovimiento> lista = null;
            IServicoEncabezado _ServiceEncabezado = new ServicioEncabezado();

            // Error porque viene en blanco 
            if (string.IsNullOrEmpty(filtro.ToString()))
            {
                lista = _ServiceEncabezado.GetEncabezadoMovimientos();
            }
            else
            {
                lista = _ServiceEncabezado.GetEncabezadoByTipo(filtro);
            }


            // Retorna un Partial View
            return PartialView("_PartialViewEncabezadoAdmin", lista);
        }






    }
}