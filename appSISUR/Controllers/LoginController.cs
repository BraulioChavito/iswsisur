﻿using ApplicationCore.SERVICES;
using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using appSISUR.Utils;

namespace appSISUR.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            if (TempData.ContainsKey("NotificationMessage"))
            {
                ViewBag.NotificationMessage = TempData["NotificationMessage"];
            }
            return View();
        }

        public ActionResult Login(Persona persona)
        {
            IServicePersona _ServiceUsuario = new ServicePersona();
            Persona oUsuario = null;
            try
            {
                //if (ModelState.IsValid)
                //{
                    oUsuario = _ServiceUsuario.GetPersonaLogin(persona.email, persona.contrasena);

                    if (oUsuario != null)
                    {
                        Session["User"] = oUsuario;
                        Log.Info($"Accede {oUsuario.nombrePersona} con el rol {oUsuario.Rol.idRol}-{oUsuario.Rol.descripcionRol} ");
                        
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        Log.Warn($"{persona.email} se intentó conectar  y falló");
                        ViewBag.NotificationMessage = Utils.SweetAlertHelper.Mensaje("Login", "Error al autenticarse", SweetAlertMessageType.warning);

                    }
                //}
                TempData["mensaje"] = Utils.SweetAlertHelper.Mensaje("Login", "Usuario autenticado satisfactoriamente", SweetAlertMessageType.success);
                return View("Index");
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                // Pasar el Error a la página que lo muestra
                TempData["Message"] = ex.Message;
                TempData.Keep();
                return RedirectToAction("Default", "Error");
            }
        }
        public ActionResult UnAuthorized()
        {
            try
            {
                ViewBag.Message = "No autorizado";

                if (Session["User"] != null)
                {
                    Persona oUsuario = Session["User"] as Persona;
                    Log.Warn($"El usuario {oUsuario.nombrePersona} con el rol {oUsuario.Rol.idRol}-{oUsuario.Rol.descripcionRol}, intentó acceder una página sin permisos  ");
                }

                return View();
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                // Pasar el Error a la página que lo muestra
                TempData["Message"] = ex.Message;
                TempData["Redirect"] = "Login";
                TempData["Redirect-Action"] = "Index";
                return RedirectToAction("Default", "Error");
            }
        }
        public ActionResult Logout()
        {
            try
            {
                Log.Info("Usuario desconectado!");
                Session["User"] = null;
                return RedirectToAction("Index", "Login");
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                // Pasar el Error a la página que lo muestra
                TempData["Message"] = ex.Message;
                TempData["Redirect"] = "Login";
                TempData["Redirect-Action"] = "Index";
                return RedirectToAction("Default", "Error");
            }
        }
    }
}
