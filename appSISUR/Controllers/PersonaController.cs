﻿using ApplicationCore.Services;
using ApplicationCore.SERVICES;
using appSISUR.Enum;
using appSISUR.Security;
using appSISUR.Utils;
using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace appSISUR.Controllers
{
    public class PersonaController : Controller
    {
        // GET: Persona
        public ActionResult Index()
        {
            IEnumerable<Persona> lista = null;
            try
            {
                if (TempData.ContainsKey("NotificationMessage"))
                {
                    ViewBag.NotificationMessage = TempData["NotificationMessage"];
                }
                IServicePersona _ServicePersona = new ServicePersona();
                lista = _ServicePersona.GetPersona();
            }
            catch (Exception ex)
            {
                //using web utils
                Log.Error(ex, MethodBase.GetCurrentMethod());
            }
            return View(lista);
        }




        public PartialViewResult Personas(int? estado)
        {
            IEnumerable<Persona> lista = null;
            IServicePersona _ServicePersona = new ServicePersona();
            if (estado != null)
            {
                lista = _ServicePersona.GetPersonaByEstado((int)estado);
            }
            return PartialView("_PartialViewPersonaInactiva", lista);
        }




        public ActionResult IndexAdmin()
        {
            IEnumerable<Persona> lista = null;
            try
            {
                IServicePersona _ServicePersona = new ServicePersona();
                lista = _ServicePersona.GetPersona();
            }
            catch (Exception ex)
            {
                //using web utils
                Log.Error(ex, MethodBase.GetCurrentMethod());
            }
            return View(lista);
        }
        [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult Details(int? id)
        {
            IServicePersona _ServicePersona = new ServicePersona();
            Persona persona = null;
            try
            {
                // Si va null
                if (id == null)
                {
                    return RedirectToAction("Index");
                }

                persona = _ServicePersona.GetPersonaById(id.Value);
                if (persona == null)
                {
                    TempData["Message"] = "No existe la persona solicitada";
                    TempData["Redirect"] = "Persona";
                    TempData["Redirect-Action"] = "IndexAdmin";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }
                ViewBag.Titulo = "Detalle Persona";
                return View(persona);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Persona";
                TempData["Redirect-Action"] = "Index";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }
        public ActionResult AjaxDetails(int id)
        {

            IServicePersona _ServicePersona = new ServicePersona();
            Persona persona = _ServicePersona.GetPersonaById(id);
            return PartialView("_PartialViewPersonaDetails", persona);
        }



      //  [CustomAuthorize((int)Roles.Administrador)]
        public ActionResult buscarPersonaxEstado(int estado)
        {
            IEnumerable<Persona> lista = null;
            IServicePersona _ServicePersona = new ServicePersona();

            // Error porque viene en blanco 
            if (string.IsNullOrEmpty(estado.ToString()))
            {
                lista = _ServicePersona.GetPersona();
            }
            else
            {
                lista = _ServicePersona.GetPersonaByEstado(estado);
            }

            // Retorna un Partial View
            return PartialView("_PartialViewUsuarioInactivo", lista);
        }




        private SelectList listaRoles(int idRol = 0)
        {
            //Lista de roles
            IServiceRol _ServiceRol = new ServiceRol();
            IEnumerable<Rol> listaRoles = _ServiceRol.GetRol();
            //Autor SelectAutor = listaAutores.Where(c => c.IdAutor == idAutor).FirstOrDefault();
            return new SelectList(listaRoles, "idRol", "descripcionRol", idRol);
        }
        private SelectList listaProveedores(int idProveedor = 0)
        {
            //Lista de Proveedores
            IServiceProveedor _ServiceProveedor = new ServiceProveedor();
            IEnumerable<Proveedor> listaProveedores = _ServiceProveedor.GetProveedor();
            return new SelectList(listaProveedores, "codProveedor", "nombreProveedor", idProveedor);
        }
        public ActionResult Create()
        {
            //Lista de roles
            ViewBag.IdRol = listaRoles();
            ViewBag.idProveedor = listaProveedores();
            ViewBag.NotificationMessage = TempData["NotificationMessage"];
            return View();
        }
        [CustomAuthorize((int)Roles.Administrador)]
        // GET: persona/Edit/5
        public ActionResult Edit(int? id)
        {
            IServicePersona _ServicePersona = new ServicePersona();
            Persona persona = null;
            try
            {
                // Si va null
                if (id == null)
                {
                    return RedirectToAction("Index");
                }

                persona = _ServicePersona.GetPersonaById(id.Value);
                if (persona == null)
                {
                    TempData["Message"] = "No existe la persona solicitada";
                    TempData["Redirect"] = "Persona";
                    TempData["Redirect-Action"] = "Index";
                    // Redireccion a la captura del Error
                    return RedirectToAction("Default", "Error");
                }
                //Lista de roles
                ViewBag.IdRol = listaRoles(persona.idRol);
                ViewBag.idProveedor = listaProveedores(persona.codProveedor.Value);
                return View(persona);
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Libro";
                TempData["Redirect-Action"] = "IndexAdmin";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }

      


        [HttpPost]
        public ActionResult Save(Persona persona)
        {            
            IServicePersona _ServicePersona = new ServicePersona();
            try
            {
                if (ModelState.IsValid)
                {
                    Persona oPersonaI = _ServicePersona.Save(persona);
                }
                else
                {
                    // Valida Errores si Javascript está deshabilitado
                    Util.Util.ValidateErrors(this);                  
                    ViewBag.IdRol = listaRoles();
                    ViewBag.idProveedor = listaProveedores();
                    return View("Create", persona);
                }
                TempData["NotificationMessage"] = SweetAlertHelper.Mensaje("Usuario", "Usuario registrado satisfactoriamente!", SweetAlertMessageType.success);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                // Salvar el error en un archivo 
                Log.Error(ex, MethodBase.GetCurrentMethod());
                TempData["Message"] = "Error al procesar los datos! " + ex.Message;
                TempData["Redirect"] = "Persona";
                TempData["Redirect-Action"] = "Index";
                // Redireccion a la captura del Error
                return RedirectToAction("Default", "Error");
            }
        }

    }
}