﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace appSISUR.Controllers
{
    public class copiarController : Controller
    {
        // GET: copiar
        public ActionResult Index()
        {
            return View();
        }

        // GET: copiar/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: copiar/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: copiar/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: copiar/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: copiar/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: copiar/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: copiar/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
