﻿using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.SERVICES
{
    public interface IServiceProducto
    {
        IEnumerable<Producto> GetProductos();
        IEnumerable<Producto> GetProductosById(int idProducto);
        IEnumerable<Producto> GetProductoByProveedor(int idProveedor);
        IEnumerable<Producto> GetProductoByNameProveedor(String nombreProveedor);
        Producto GetProductoById(int id);
        Producto GetProductoByCodigo(int id);
        Producto GetProductoByProveedores(int id);
        Producto GetProductoByMarca(String marca);
        IEnumerable<Producto> GetProductoByNombreProducto(String nombre);
        IEnumerable<string> GetProductoNombres();
        Producto save(Producto producto, string[] selectedProveedores);
        void DeleteProducto(int id);

        int totalProductos();
    }
}
