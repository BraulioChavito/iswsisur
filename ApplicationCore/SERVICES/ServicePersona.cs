﻿using ApplicationCore.Utils;
using Infraestucture.Models;
using Infraestucture.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.SERVICES
{
    public class ServicePersona : IServicePersona
    {
        public IEnumerable<Persona> GetPersona()
        {
            IRepositorioPersona repositoryPersona = new RepositorioPersona();
            return repositoryPersona.GetPersona();
        }

        public Persona GetPersonaByCod(int cod)
        {
            IRepositorioPersona repository = new RepositorioPersona();
            Persona oPersona = repository.GetPersonaByCod(cod);
           oPersona.contrasena = Cryptography.DecrypthAES(oPersona.contrasena);
            return oPersona;
        }

        public IEnumerable<Persona> GetPersonaByEstado(int estado)
        {
            IRepositorioPersona repositoryPersona = new RepositorioPersona();

            return repositoryPersona.GetPersonaByEstado(estado);
        }

        public Persona GetPersonaById(int id)
        {
            IRepositorioPersona repository = new RepositorioPersona();
            Persona oPersona = repository.GetPersonaById(id);
           oPersona.contrasena = Cryptography.DecrypthAES(oPersona.contrasena);
            return oPersona;
        }


        public Persona GetPersonaByNombre(string nombrePersona)
        {
            throw new NotImplementedException();
        }

        public Persona GetPersonaByRol(int rol)
        {
            throw new NotImplementedException();
        }

        public Persona GetPersonaLogin(string email, string password)
        {
            IRepositorioPersona repository = new RepositorioPersona();

            // Encriptar el password para poder compararlo
            string crytpPasswd = Cryptography.EncrypthAES(password);

            return repository.GetPersonaLogin(email, crytpPasswd);
           
        }

        public Persona Save(Persona persona)
        {
            IRepositorioPersona repository = new RepositorioPersona();
           persona.contrasena = Cryptography.EncrypthAES(persona.contrasena);
            return repository.Save(persona);
        }
    }
}

