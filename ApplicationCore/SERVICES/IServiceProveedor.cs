﻿using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.SERVICES
{
    public interface IServiceProveedor
    {
        IEnumerable<Proveedor> GetProveedor();
        IEnumerable<Proveedor> GetLibroByIdPersona(int idPersona);
        Proveedor GetProveedorByCodigo(int codigo);
        Proveedor GetProveedorById(int Id);
        IEnumerable<Proveedor> GetProveedorByNombre(String nombre);
        IEnumerable<string> GetProveedoresNombres();
        Proveedor GetProveedorByestado(int estado);
        Proveedor SaveProveedor(Proveedor proveedor);
    }
}
