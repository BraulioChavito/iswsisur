﻿using Infraestucture.Models;
using Infraestucture.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.SERVICES
{
   public class ServiceProducto : IServiceProducto
    {
        public void DeleteProducto(int id)
        {
            IRepositorioProducto repository = new repositorioProducto();
             repository.DeleteProducto(id);
        }

        public Producto GetProductoByCodigo(int id)
        {
            IRepositorioProducto repository = new repositorioProducto();
            return repository.GetProductoByCodigo(id);
        }

        public Producto GetProductoById(int id)
        {
            IRepositorioProducto repository = new repositorioProducto();
            return repository.GetProductoById(id);
        }

        public Producto GetProductoByMarca(string marca)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Producto> GetProductoByNameProveedor(string nombreProveedor)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Producto> GetProductoByNombreProducto(string nombre)
        {
            IRepositorioProducto repository = new repositorioProducto();
            return repository.GetProductoByNombreProducto(nombre);
        }

        public IEnumerable<Producto> GetProductoByProveedor(int idProveedor)
        {
            IRepositorioProducto repository = new repositorioProducto();
            return repository.GetProductoByProveedor(idProveedor);
        }

        public IEnumerable<Producto> GetProductoByProveedor(string nombreProveedor)
        {
            IRepositorioProducto repository = new repositorioProducto();
            return repository.GetProductoByNameProveedor(nombreProveedor);  
        }

        public Producto GetProductoByProveedores(int id)
        {
            IRepositorioProducto repository = new repositorioProducto();
            return repository.GetProductoByProveedores(id);
        }

        public IEnumerable<string> GetProductoNombres()
        {
            IRepositorioProducto repository = new repositorioProducto();
            return repository.GetProductos().Select(x => x.descripcionProducto);
        }

        public IEnumerable<Producto> GetProductos()
        {
            IRepositorioProducto repositoryProducto = new repositorioProducto();
            return repositoryProducto.GetProductos();
        }

        public IEnumerable<Producto> GetProductosById(int idProducto)
        {
            IRepositorioProducto repositoryProducto = new repositorioProducto();
            return repositoryProducto.GetProductosById(idProducto);
        }

        public Producto save(Producto producto, string[] selectedProveedores)
        {
            IRepositorioProducto repository = new repositorioProducto();
            return repository.save(producto,selectedProveedores);
        }

        public int totalProductos()
        {
            IRepositorioProducto repository = new repositorioProducto();
            return repository.totalProductos();
        }
    }
}
