﻿using Infraestucture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.SERVICES
{
   public interface IServicePersona
    {
        IEnumerable<Persona> GetPersona();
        Persona GetPersonaById(int id);
        Persona GetPersonaByCod(int cod);
        IEnumerable<Persona> GetPersonaByEstado(int estado);
        Persona GetPersonaByNombre(String nombrePersona);
        Persona GetPersonaByRol(int rol);
        Persona GetPersonaLogin(string email, string password);
        Persona Save(Persona persona);
    }
}
